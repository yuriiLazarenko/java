package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class HomePage {

    public WebDriver driver;

    @FindBy(id = "search")
    public WebElement fieldSearch;

    @FindBy(id = "search-icon-legacy")
    public WebElement searchButton;

    @FindBy(xpath = "//*[@id=\"button\"]/*[contains(text(),'Sign in')]")
    public WebElement signIn;

    @FindBy(id = "items")
    public WebElement items;

    @FindBy(id = "guide-button")
    public WebElement guideButton;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void search(String seartchWord){
        fieldSearch.sendKeys(seartchWord);
        searchButton.submit();
    }

    public void login(){
        signIn.click();
    }

    public List<WebElement> getMiniMenuItems(){
        guideButton.click();
        List<WebElement> list = items.findElements(By.xpath("//ytd-mini-guide-entry-renderer/a[@id=\"endpoint\"]"));
        return list;
    }







}
