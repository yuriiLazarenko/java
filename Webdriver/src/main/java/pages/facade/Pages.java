package pages.facade;

import org.openqa.selenium.WebDriver;
import pages.HomePage;
import pages.SignInPage;


public class Pages {
    private WebDriver webDriver;
    private HomePage homePage;
    private SignInPage signInPage;

    public HomePage getHomePage() { return (homePage==null)?new HomePage(webDriver):homePage; }
    public SignInPage getSignInPage() { return (signInPage==null)?new SignInPage(webDriver):signInPage; }

    public Pages(WebDriver driver) { webDriver = driver;}
    public WebDriver getWebDriver() {
        return webDriver;
    }
    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }
}
