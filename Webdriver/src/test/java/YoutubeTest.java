import basetest.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.List;

import static basetest.Base.HOME_URL;


public class YoutubeTest {

    public String SearchWord = "Знайдер";
    private Base Pages;

    public YoutubeTest() {
        Pages = new Base();
    }

    @BeforeTest
    public void setUp() {
        Pages.getWebDriver().navigate().to(HOME_URL);
    }

    @Test
    public void testForSearch() {
        Pages.getPages().getHomePage().search(SearchWord);
        List<WebElement> list = Pages.getWebDriver().findElements(By.xpath("//*[contains(text(),'" + SearchWord + "')]"));
        Assert.assertTrue(list.size() > 0, "Text not found!");
    }

    @Test
    public void loginPageIsOpened() {
        Pages.getPages().getHomePage().login();
        Assert.assertEquals(Pages.getPages().getSignInPage().getSignInTitle().getText(),"Sign in\n" + "to continue to YouTube");
    }

    @Test
    public void miniMenu() {
        List<WebElement> menuItems = Pages.getPages().getHomePage().getMiniMenuItems();
        Assert.assertEquals(menuItems.get(0).getText(),"Home");
        Assert.assertEquals(menuItems.get(1).getText(),"Trending");
        Assert.assertEquals(menuItems.get(2).getText(),"Subscriptions");
        Assert.assertEquals(menuItems.get(3).getText(),"Library");
        Assert.assertEquals(menuItems.get(4).getText(),"History");
    }

    @AfterTest
    public void quitDriver() {
        Pages.close();
    }


}
