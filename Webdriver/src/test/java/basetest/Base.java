package basetest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import pages.facade.Pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Base {

    public static final String CHROME = "chrome";
    public static final String OPERA = "opera";
    public static final String FIREFOX = "firefox";
    public static final String HOME_URL;

    private static  Pages allWebSitePages;
    private static Properties generalProperties;

    protected WebDriver webDriver;

    static {
        generalProperties = new Properties();
        FileInputStream fis;
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            File file = new File(classLoader.getResource("properties.properties").getFile());
            fis = new FileInputStream(file);
            generalProperties.load(fis);

            String nameSelenium = generalProperties.getProperty("selenium.name");
            String addressSelenium = generalProperties.getProperty("selenium.address");
            System.setProperty(nameSelenium, addressSelenium);
        } catch (IOException e) {
            System.err.println("Error: file is not exist!");

        }
        HOME_URL = generalProperties.getProperty("project.URL");
    }

    public Base() {
        switch (generalProperties.getProperty("driver")) {
            case CHROME:
                webDriver = new ChromeDriver();
                break;
            case OPERA:
                OperaOptions operaOptions = new OperaOptions();
                operaOptions.setBinary(generalProperties.getProperty("opera.binary"));
                webDriver = new OperaDriver(operaOptions);
                break;
            case FIREFOX:
                webDriver = new FirefoxDriver();
                break;
            default:
                webDriver = null;
        }

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();

    }

    public  Pages getPages() {
        return (allWebSitePages==null)?new Pages(getWebDriver()):allWebSitePages;
    }
    public WebDriver getWebDriver(){
        return webDriver;
    }

    public void close(){
        webDriver.quit();
        webDriver = null;
    }

    public static Properties getGeneralProperties() {
        return generalProperties;
    }

    public static void setGeneralProperties(Properties generalProperties) {
        Base.generalProperties = generalProperties;
    }
}

