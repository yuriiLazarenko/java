package com.softserve.edu;

import org.junit.Test;

import static com.softserve.edu.Utils.verifyEnvelopesInserting;
import static org.junit.Assert.assertEquals;

public class UtilsTest {
    @Test
    public void verifyEnvelopeInsertingTest() {
        Envelope e1 = new Envelope(2, 5);
        Envelope e2 = new Envelope(7, 1);
        assertEquals("wrong result", "Any Envelope can be inserted", verifyEnvelopesInserting(e1, e2));

        e1 = new Envelope(4, 4);
        e2 = new Envelope(4, 4);
        assertEquals("wrong result", "Envelopes are the same", verifyEnvelopesInserting(e1, e2));

        e1 = new Envelope(4.2, 78.4);
        e2 = new Envelope(77.5, 4.1);
        assertEquals("wrong result", "Second Envelope can insert into first", verifyEnvelopesInserting(e1, e2));

        e1 = new Envelope(77.5, 78.4);
        e2 = new Envelope(79, 78);
        assertEquals("wrong result", "First Envelope can insert into second", verifyEnvelopesInserting(e1, e2));
    }
}