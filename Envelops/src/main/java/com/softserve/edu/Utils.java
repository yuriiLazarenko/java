package com.softserve.edu;

public class Utils {
    static String verifyEnvelopesInserting(Envelope envelope1, Envelope envelope2) {
        String msg = new String();
        envelope1.rotateSides();
        envelope2.rotateSides();
        if ((envelope1.getA() > envelope2.getA()) && (envelope1.getB() > envelope2.getB())) {
            msg = "Second Envelope can insert into first";
        } else if ((envelope1.getA() < envelope2.getA()) && (envelope1.getB() < envelope2.getB())) {
            msg = "First Envelope can insert into second";
        } else if ((envelope1.getA().equals(envelope2.getA())) && (envelope1.getB().equals(envelope2.getB()))) {
            msg = "Envelopes are the same";
        } else {
            msg = "Any Envelope can be inserted";
        }
        return msg;
    }
}
