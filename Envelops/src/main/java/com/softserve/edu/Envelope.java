package com.softserve.edu;

import java.util.Scanner;

public class Envelope {
    private Double a, b;

    Envelope() {
    }

    Envelope(double firstSide, double secondSide) {
        setA(firstSide);
        setB(secondSide);
        rotateSides();
    }

    void init() {
        Scanner scaner = new Scanner(System.in);
        System.out.print("Insert envelope first side:");
        setA(scaner.nextDouble());
        System.out.print("Insert envelope first side:");
        setB(scaner.nextDouble());
        System.out.println("first side:" + a + " second side:" + b);
        rotateSides();
    }

    public Double getA() {
        return a;
    }

    public void setA(Double a) {
        this.a = a;
    }

    public void setB(Double b) {
        this.b = b;
    }

    public Double getB() {
        return b;
    }

    void rotateSides() {
        Double aNew, bNew;
        if (a > b) {
            aNew = b;
            bNew = a;
            a = aNew;
            b = bNew;
        }
    }
}
