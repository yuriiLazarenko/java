package com.softserve.edu;

import java.io.IOException;
import java.util.Scanner;

import static com.softserve.edu.Utils.verifyEnvelopesInserting;

public class Main {

    public static void main(String[] args) throws IOException {
        String isContinue = "y";
        while (isContinue.equalsIgnoreCase("y") || isContinue.equalsIgnoreCase("yes")) {
            Envelope envelope1 = new Envelope();
            Envelope envelope2 = new Envelope();
            envelope1.init();
            envelope2.init();
            verifyEnvelopesInserting(envelope1, envelope2);
            System.out.println("Do you want to continue?(y/yes)");
            Scanner scanner = new Scanner(System.in);
            isContinue = scanner.nextLine();
            System.out.println(isContinue);
        }
    }
}
